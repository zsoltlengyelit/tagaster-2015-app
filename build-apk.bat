@ECHO OFF

SET builddir=%~dp0
SET appname=TagasTer

cordova build --release android

jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore %builddir%\app-release-key.keystore android-release-unsigned.apk tagas_ter_app_cert

zipalign -v 4 %builddir%\platforms\android\build\outputs\apk\android-release-unsigned.apk %builddir%\platforms\android\build\outputs\apk\%appname%-%DATE:~-4%.%DATE:~4,2%.%DATE:~7,2%.apk
