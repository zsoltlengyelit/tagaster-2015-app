// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic','ionic.service.core', 'starter.controllers', 'starter.services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {

    // if(window.Connection) {

    //   if(navigator.connection.type != Connection.NONE
    //   && navigator.connection.type != Connection.UNKNOWN
    //   && navigator.connection.type != Connection.CELL_2G) {
    //     // register analiticy when internet is available
    //     //$ionicAnalytics.register();
    //     window.$ionicAnalytics = $ionicAnalytics;
    //     $ionicAnalytics.ACTIVE = false;
    //   }
    // }

    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleLightContent();
    }
  });
})

.filter('empty', function(){
    return function(data){
      return null == data ||  Object.keys(data).length === 0;
    }
  })

.factory('$exceptionHandler', [function () {
  return function errorCatcherHandler(exception, cause) {
    console.error(exception.stack);
    if(window.$ionicAnalytics && window.$ionicAnalytics.ACTIVE){
      window.$ionicAnalytics.track('AppError', {
        exception: exception,
        cause: cause
      })
    }
  };
}])

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

  //$ionicConfigProvider.views.transition('none');

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

    .state('app', {
      url: '/app',
      abstract: true,
      templateUrl: 'menu.html',
      controller: 'MenuCtrl'
    })

    .state('event', {
      url : '/event/:id',
      templateUrl : 'templates/event.html',
      controller : 'EventCtrl'
    })

  // Each tab has its own nav history stack:

  .state('app.locations', {
    url: '/location/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/events.html',
        controller: 'LocationCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/location/1');

});
