(function () {
  var Programs = [];
  var Events = {}; // by Id
  var progId = 0;
  var eventId = 0;

  /**
   * Date parser
   * @param datestring
   * @returns {Date}
   */
  function d(datestring) {

    var end = undefined;
    if (datestring.indexOf('-') > -1) {
      var parts = datestring.split('-');

      end = new Date('2015-09-' + parts[1] + ':00')
      datestring = parts[0];
    }

    return {start: new Date('2015-09-' + datestring + ':00'), end: end};
  }

  function addLoc(place) {
    place.id = ++progId;

    if (place.programs) {
      place.programs.forEach(function (item) {

        item.placeName = place.name;
        item.placeId = place.id;
        item.id = ++eventId;

        Events[item.id] = item;

        if (item.date) {
          item.startDate = new Date(item.date.start);
          item.startDate.setHours(0, 0, 0, 0);

          item.startDateString = 'Szept. ' + item.startDate.getDate() + '.'; // groupBy filter uses strings
        }
      });
    }

    Programs.push(place);
  }

  function initEvents(eventsByPlace) {

    // add from events
    for (var loci = 0; loci <eventsByPlace.length; loci++) {
      var loc = eventsByPlace[loci];
      var progs = [];

      loc.programs.forEach(function (event) {
        progs.push({
          date: d('' + event.day + ' ' + event.start.substring(0, 5) +
            (event.end ? ('-' + event.day + ' ' + event.end.substring(0, 5) ) : '')
          ),
          title: event.name,
          description: event.description
        });
      });

      addLoc({
        name: loc.name,
        programs: progs
      });

    }

  }

  initEvents(window.EVENTS_OF_APP);

  angular.module('starter.programs', [])
    .value('Events', Events)
    .value('Programs', Programs);

})();
