(function(){
  "use strict";

  var MORNING = 'morning';
  var AFTERNOON = 'afternoon';
  var EVENING = 'evening';

  var DAYS = [
      {name : 'Szept. 25.', date: 25},
      {name : 'Szept. 26.', date: 26},
      {name : 'Szept. 27.', date: 27}
    ];

  var DAYINTERVALS = {};
  DAYINTERVALS[MORNING] = 'Délelőtt';
  DAYINTERVALS[AFTERNOON] = 'Délután';
  DAYINTERVALS[EVENING] = 'Este';

  // /**
  //  * Track with analitics.
  //  * @param $ionicAnalytics
  //  * @param event
  //  * @param data
  //  */
  // function trackEvent($ionicAnalytics, event, data) {
  //   if($ionicAnalytics && $ionicAnalytics.ACTIVE == true){
  //     $ionicAnalytics.track(event, data);
  //   }
  // }


angular.module('starter.controllers', [])

  .filter('menuplace', function(){
    return function(placename){
      return placename.replace(/\(.*\)/, '');
    }
  })

  .controller('MenuCtrl', ['$scope', '$rootScope', 'titleService', 'Locations', 'appTitle', 'filterService', function ($scope, $rootScope, titleService, Locations, appTitle, filterService) {

    titleService.set(appTitle);

    $scope.titleService = titleService;
    $scope.Locations = Locations;
    $scope.filterService = filterService;

    $scope.days = DAYS;

    $scope.dayIntervals = DAYINTERVALS;

    $scope.$watch(function(){return filterService.day;}, function(){
      $rootScope.$emit('reloadEvents', 'date');
    });
    $scope.$watch(function(){return filterService.dayInterval;}, function(){
      $rootScope.$emit('reloadEvents', 'date');
    });


    $scope.jumpToLocation = function(location){
      filterService.reset();
      $rootScope.$emit('reloadEvents', 'place');
    };


  }])

  .controller('EventCtrl', ['$scope', '$stateParams', 'Events', '$ionicNavBarDelegate', '$ionicHistory', '$state', '$rootScope', '$timeout', 'filterService',
    function($scope, $stateParams, Events, $ionicNavBarDelegate, $ionicHistory, $state, $rootScope, $timeout, filterService){
      var id = $stateParams.id;
      $scope.event = Events[id];

      $scope.back = function(){
          if($ionicHistory.backView()){
            $ionicHistory.goBack();
          }else{
            $state.go('app.locations'); // just for dev
          }
          $timeout(function(){
            $rootScope.$emit('reloadEvents', 'back');
          });
      };

      $scope.goToPlace = function(placeId){
          filterService.reset();
          $state.go('app.locations', {id : placeId});
      };

  }])

  .filter('striphtml', function(){
    return function(html){
        var tmp = document.createElement("DIV");
        tmp.innerHTML = html;
        return tmp.textContent || tmp.innerText || "";
    }
  })

  .controller('LocationCtrl', ['$scope', '$timeout', 'ttime', '$state', '$rootScope', 'util', 'Programs', 'titleService', '$stateParams', 'filterService', '$ionicScrollDelegate',
    function ($scope, $timeout, ttime, $state, $rootScope, util, Programs, titleService, $stateParams, filterService, $ionicScrollDelegate) {

    $scope.filterService = filterService;
    $scope.titleService = titleService;

    $scope.filterByText = function(){
      reload();
    };

    $scope.clearFilter = function(){
      filterService.reset();
      reload();
    };

      $scope.showEvent = function(event){
        $state.go('event', {id : event.id});
      };

    function reload(selectionType) {

      $scope.programEvents = {};
      function addToProgramGroup(event){
        var groupName = event.startDateString;
        if(!(groupName in $scope.programEvents)){
          $scope.programEvents[groupName] = [];
        }
        $scope.programEvents[groupName].push(event);
      }

      if((selectionType == 'date' || selectionType == 'back') && filterService.isFilterByDate()){

        // trackEvent($ionicAnalytics, 'Filter by Date', {
        //   expression: filterService._getTimeTimeInterval().start
        // });

        filterService.filterText = '';

        Programs.forEach(function(loc){
          loc.programs.forEach(function(event){
            if(filterService.matchDate(event.date.start)){
              addToProgramGroup(event);
            }
          });
        });

        var dayName = DAYS.filter(function(item){
          return item.date == filterService.day;
        })[0].name;
        var dayIntervalName = DAYINTERVALS[filterService.dayInterval];

        angular.forEach($scope.programEvents, function(item){
          item.sort(function(a, b){
            return a.date.start - b.date.start;
          });
        });

        titleService.set(dayName +' - ' + dayIntervalName);

      }else if(filterService.isFilterByText()){

        // trackEvent($ionicAnalytics, 'Filter by Text', {
        //   expression: filterService.filterText
        // });

        filterService.resetSelectedDay();

        Programs.forEach(function(loc){
          loc.programs.forEach(function(event){
            if(filterService.matchText(event.title)){
              addToProgramGroup(event);
            }
          });
        });

        titleService.setAllPlace();

      } else if(filterService.isFilterByPlace()){

        var locationId = $stateParams.id || 1; // default is 1
        var location = Programs.filter(function (item) {
          return item.id == locationId;
        })[0];

        // trackEvent($ionicAnalytics, 'Filter by Place', {
        //   expression: location.name
        // });

        location.programs.forEach(function(event){
          addToProgramGroup(event);
        });

        titleService.set(location.name);

      }

      $ionicScrollDelegate.scrollTop();
    }
    reload();


    $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
      reload();
    });
    $rootScope.$on('reloadEvents', function(event, selectionType){
      reload(selectionType);
    })
  }]);

})();
